from hopper_interface import *
import sys
import time
hopper = HopperInterface('enp14s0')
try:
    while True:
        state = hopper.getState()
        if state is not None:
            leg_coords = motorTelemetryToLegCoords(state)
            print('r %f\ntheta %f' % leg_coords)
            time.sleep(1e-1)
except:
    sys.exit(1)
