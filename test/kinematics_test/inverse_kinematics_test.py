from hopper_interface import *
import keyboard
import argparse
import time

desc = "Implements a leg length controller for demonstration and testing purposes."
parser = argparse.ArgumentParser(description=desc)
parser.add_argument('length', help="percentage of leg stroke, 0 is minimum length, 1 is maximum length")

min_length = 4*.0254
max_length = 12*.0254

class PID:
    """ Basic PID controller with integral decay """
    def __init__(self, p, i, d):
        self.p = p
        self.i = i 
        self.d = d 
        self.decay = 0.02
        self.integral = 0.
        self.last_error = None

    def update(self, error, dt):
        self.integral = (1-self.decay)*self.integral + dt * error
        derivative = 0.
        if self.last_error is not None:
            derivative = (error - self.last_error) / dt
        self.last_error = error
        output = self.p * error + self.i * self.integral + self.d * derivative
        return output
    
    def reset_integral(self):
        self.integral = 0.

    def reset_derivative(self):
        self.last_error = None

# create PIDS for controlling leg angle
leg_m1_pid = PID(.05, 0., 1e-3)
leg_m2_pid = PID(.05, 0., 1e-3)

if __name__ == "__main__":

    args = parser.parse_args()
    percent = float(args.length)
    target_length = min_length + (max_length-min_length)*percent
    target_leg_angle = 3.14159265/2
    x = legCoordsToMotorAngles(target_length, target_leg_angle)
    m1_target_angle = x[0]
    m2_target_angle = x[1]

    hopper = HopperInterface('enp14s0')

    try:
        then = time.time()
        while True:
            state = hopper.getState()
            if state is not None:
                now = time.time()
                dt = now - then
                then = now
                m1_error = m1_target_angle-state.leg_m1_angle
                m2_error = m2_target_angle-state.leg_m2_angle
                leg_m1_effort = leg_m1_pid.update(m1_error, dt)
                leg_m2_effort = leg_m2_pid.update(m2_error, dt)
                hopper.setLegM1Effort(leg_m1_effort)
                hopper.setLegM2Effort(leg_m2_effort)
                hopper.updateMotorCommand()
                
                if keyboard.is_pressed('enter'):
                    break
        hopper.stopAllMotors()
        time.sleep(1.)
        hopper.disconnect()
    except BaseException as e:
        print("caught exception " + str(e) + "\n Stopping program now!")
        print(type(e))
        hopper.stopAllMotors()
        time.sleep(1)
        hopper.disconnect()