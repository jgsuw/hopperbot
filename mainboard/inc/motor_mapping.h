#ifndef HOPPER_MOTOR_MAPPING_H
#define HOPPER_MOTOR_MAPPING_H

#include <Motor.h>
/**
 * FIXME mapping of leg motors to the global joint table
 * These constants depend on the order in which the motors are connected by ethernet!
 * In this case, the order is mainboard --> leg motor 1 --> leg motor 2
 */
/* Indices of leg motors in joint table */
extern const int leg_m1_idx = 5;
extern const int leg_m2_idx = 7;
extern const int haptic_m_idx = 6;

/* Leg direction mapping */
extern const int leg_m1_dir = 1;
extern const int leg_m2_dir = -1;
extern const int haptic_m_dir = 1;

/* Leg motor zero angle offsets */
//extern const float leg_m1_offset = -.497795;
extern const float leg_m1_offset = -3.43+.5; // Joey was here
extern const float leg_m2_offset = 2.265;    // I really need to fix this shit
extern const float haptic_m_offset = 1.4983341693878174;

/* Joint pointers */
extern Joint * const leg_m1 = &joint[leg_m1_idx];
extern Joint * const leg_m2 = &joint[leg_m2_idx];
extern Joint * const haptic_m = &joint[haptic_m_idx];

#endif
