/**
 * Written by Joseph Sullivan
 * email: jgs6156@uw.edu
 */
#ifndef HOPPER_COMMANDS_H
#define HOPPER_COMMANDS_H

#define JOEY_MAGIC 31

enum {
  motor_command = 42
};

class GenericCommand {
public:
  GenericCommand(uint8_t * raw_data) {
    magic = raw_data[0];
    code = raw_data[1];
    memcpy(data, raw_data+2, sizeof(data));
  }
  uint8_t code;
  uint8_t magic;
  uint8_t data[32];
};

class MotorCommand {
public:
  MotorCommand(GenericCommand cmd) {
    this->mode_mask = cmd.data[0];
    memcpy(this->control_vector, cmd.data+1, sizeof(this->control_vector));
  }
  uint8_t mode_mask;
  float control_vector[3];
};

class SetMotorPIGain {
public:
  SetMotorPIGain(GenericCommand cmd) {
    this->motor_idx = cmd.data[0];
    memcpy(&this->Kp, cmd.data+1, sizeof(this->Kp));
    memcpy(&this->Ki, cmd.data+5, sizeof(this->Ki));
  }
  uint8_t motor_idx;
  float Kp;
  float Ki;
};

#endif
