/**
 * Written by Joseph Sullivan <jgs6156@uw.edu>
 */
#include <stdio.h>
#include <SDK.h>
#include <Motor.h>
#include "motor_mapping.h"
#include "hopper_commands.h"

#define HOPPER_PROTOCOL_VERSION 42

uint8_t debug_buf[34];

float leg_m1_angle;
float leg_m2_angle;
float haptic_m_angle;
float leg_m1_v;
float leg_m2_v;
float haptic_m_v;
uint32_t timestamp;

class EthernetReceiver : public Behavior
{

public:

    void begin()
    {
        /* Set the PD gain of the three motors */
        leg_m1->setGain(.25, 0.0);
        leg_m2->setGain(.25, 0.0);
        haptic_m->setGain(0.3, 0.0);
    }

    /**
     * Decodes commands from upstream ethernet interface and then executes them.
     * Also has some basic input sanitation
     * @param cmd generic command object
     */
    void handleCommand(GenericCommand generic_cmd){
        if (generic_cmd.code == motor_command) {
            MotorCommand cmd(generic_cmd);
            if (cmd.mode_mask & (1 << leg_m1_idx)) { leg_m1->setPosition(cmd.control_vector[0]); }
            else { leg_m1->setOpenLoop(cmd.control_vector[0]); }
            if (cmd.mode_mask & (1 << leg_m2_idx)) { leg_m2->setPosition(cmd.control_vector[1]); }
            else { leg_m2->setOpenLoop(cmd.control_vector[1]); }
            if(cmd.mode_mask & (1 << haptic_m_idx)) { haptic_m->setPosition(cmd.control_vector[2]); }
            else { haptic_m->setOpenLoop(cmd.control_vector[2]); }
        }

    }

    /**
     * Main working function of the ethernet receiver
     */
    void update()
    {
        leg_m1_angle = leg_m1->getPosition();
        leg_m2_angle = leg_m2->getPosition();
        haptic_m_angle = haptic_m->getPosition();
        leg_m1_v = leg_m1->getVelocity();
        leg_m2_v = leg_m2->getVelocity();
        haptic_m_v = haptic_m->getVelocity();
        timestamp ++;
        uint8_t buf[34];
        int result = read(ETH_UPSTREAM_FILENO, &buf, sizeof(buf));
        if (result > 0) {
            memcpy(debug_buf, buf, sizeof(debug_buf));
            GenericCommand cmd(buf);
            if (cmd.magic == JOEY_MAGIC) {
                handleCommand(cmd);
            }
        }
    }

    void end()
    {

    }
};

void cd()
{

}


/**
 * Replaces the standard state copy callback with one that transmits a custom
 * telemetry data structure
 * @param  hdr pointer to header data
 * @param  buffer to place data payload into
 * @return     the size of the total UDP datagram
 */
uint16_t myStateCopyCallback(GRMHeader *hdr, uint8_t *buf)
{
    // Set your own version to keep track of parsing ability
    hdr->version = HOPPER_PROTOCOL_VERSION;
    int offset = 0;

    memcpy(&buf[offset], &leg_m1_angle, sizeof(leg_m1_angle));
    offset += sizeof(leg_m1_angle);

    memcpy(&buf[offset], &leg_m2_angle, sizeof(leg_m2_angle));
    offset += sizeof(leg_m2_angle);

    memcpy(&buf[offset], &haptic_m_angle, sizeof(haptic_m_angle));
    offset += sizeof(haptic_m_angle);

    memcpy(&buf[offset], &leg_m1_v, sizeof(leg_m1_v));
    offset += sizeof(leg_m1_v);

    memcpy(&buf[offset], &leg_m2_v, sizeof(leg_m2_v));
    offset += sizeof(leg_m2_v);

    memcpy(&buf[offset], &haptic_m_v, sizeof(haptic_m_v));
    offset += sizeof(haptic_m_v);

    memcpy(&buf[offset], &timestamp, sizeof(timestamp));
    offset += sizeof(timestamp);

    // Need to return the size at the end
    return offset;
}

/*************************************************************/
void debug() {
    if(debug_buf[0] != JOEY_MAGIC) {
        printf("bad command with code ");
        printf("%d", int(debug_buf[0]));
        printf("\n");
    } else {
        GenericCommand cmd(debug_buf);
        if (cmd.code == motor_command) {
            MotorCommand cmd2(cmd);
            printf("motor command ");
            printf("%u, %f, %f, %f\n", cmd2.mode_mask, cmd2.control_vector[0], cmd2.control_vector[1], cmd2.control_vector[2]);
        }
    }
}

/*************************************************************/

int main(int argc, char *argv[])
{

    //The following lines are REQUIRED!!!

    // Loads some default settings including motor parameters
#if defined(ROBOT_MINITAUR)
    init(RobotParams_Type_MINITAUR, argc, argv);
#elif defined(ROBOT_MINITAUR_E)
    init(RobotParams_Type_MINITAUR_E, argc, argv);
#else
#error "Define robot type in preprocessor"
#endif

    #define NUM_MOTORS 8 // magic number (the number of motors in a complete Minotaur)
    P->joints_count = S->joints_count = C->joints_count = NUM_MOTORS;
    /**
     * Configure the joints according to the motor constants defined at the top of the file
     */

    P->joints[leg_m1_idx].zero = leg_m1_offset;
    P->joints[leg_m1_idx].direction = leg_m1_dir;
    P->joints[leg_m1_idx].gearRatio = 1;

    P->joints[leg_m2_idx].zero = leg_m2_offset;
    P->joints[leg_m2_idx].direction = leg_m2_dir;
    P->joints[leg_m2_idx].gearRatio = 1;

    P->joints[haptic_m_idx].zero = haptic_m_offset;
    P->joints[haptic_m_idx].direction = haptic_m_dir;
    P->joints[haptic_m_idx].gearRatio = 1;

    // No limbs, only 1DOF joints
    P->limbs_count = 0;
    safetyShutoffEnable(false);

  // Replace state copy callback to send back custom logging info (via ethernet)
    ioctl(ETH_UPSTREAM_FILENO, IOCTL_CMD_STATE_COPY_CALLBACK, (void *)myStateCopyCallback);

    // Remove default behaviors from behaviors vector, create, add, and start ours

    //ADD YOUR BEHAVIOR HERE!!!!
    EthernetReceiver eth_receiver;
    behaviors.clear();
    behaviors.push_back(&eth_receiver);
    eth_receiver.begin();

    // setDebugRate(.01);

    return begin();
}
