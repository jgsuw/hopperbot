"""
Author: Joseph Sullivan
Contact: jgs6156@uw.edu
"""

from matplotlib import pyplot as plt
import numpy as np
import argparse

description =   """ This file can be used to analyse data provided by spring-mass-damper.py
                    Data from an experiment is parsed from the CSV file, and can be plotted, manipulated, etc in the __main__ block.
                """
parser = argparse.ArgumentParser()
parser.add_argument('file')

# indices of data columns
position_idx = 0 
velocity_idx = 1
force_idx = 2
haptic_m_effort_idx = 3
time_idx = 4
dt_idx = 5
pulley_radius = 1.5 * 2.54 / 100

if __name__=="__main__":
    # parse the input data
    args = parser.parse_args()
    infile = open(args.file, 'r')
    parts = infile.readline().rstrip('\n').split(',')
    mass = float(parts[0])
    natural_frequency = float(parts[1])
    damping_ratio = float(parts[2])
    zero_position = float(parts[3])

    # get the rest of the data
    data = map(lambda x: map(lambda y: float(y), x.rstrip('\n').split(',')), infile)
    n = len(data)
    m = len(data[0])
    data = np.reshape(np.asarray(data), (n,m))

    # make some plots
    plt.plot(data[:,time_idx], data[:,position_idx])
    plt.show()