"""
Author: Joseph Sullivan
Contact: jgs6156@uw.edu
"""

import hopper_interface as hopif
import time
import numpy as np
import keyboard
import argparse

description =   """
                This example attempts to render a spring-mass-damper system by rendering the spring and dashpot force using the haptic motor.
                However, currently the viscious friction of the cart is not known (this quantity constitutes a parallel dash-pot element).
                Thus, the damping ratio is not accurately rendered: however, the system effectively increases the viscious friction by a definite amount,
                and therefore the overall damping ratio.
                """
parser = argparse.ArgumentParser(description=description)
parser.add_argument('natural_frequency', help='the natural frequency of the spring-mass-damper system in Hz')
parser.add_argument('damping_ratio', help='the damping ratio of the spring-mass-damper system (dimensionless')

if __name__ == "__main__":

    # Open connection to hopper
    hopper = hopif.HopperInterface('enp14s0')
    hopper.setHapticMEffort(0.)
    hopper.updateMotorCommand()

    # Initialize state filter
    state_filter = hopif.StateMedianFilter(3)

    # Open output file for data logging
    filename = raw_input("Please provide filename: ")
    f = open(filename, 'w')
    foo = raw_input("press anything to begin")

    # Set up spring-mass-model
    args = parser.parse_args()
    pulley_radius = 1.5 * 2.54 / 100
    median_state = state_filter.getMedian()
    natural_frequency = float(args.natural_frequency)
    damping_ratio = float(args.damping_ratio)
    mass = 2.25
    spring_constant = mass*(6.28*natural_frequency)**2 # Must convert natural frequency from Hz to radians/s
    drag_constant = damping_ratio * 2 * (spring_constant * mass)**.5
    time.sleep(1.)

    # Get initial hopper state
    state = None
    cnt = 0
    while True:
        state = hopper.getState()
        if state is not None:
            state_filter.insert(state)
            cnt+=1
        if cnt == state_filter._window_len:
            break

    """
    Raise the robot to a specified height (as represented by the total angle of the haptic motor)
    """
    ff_effort = .12
    target_haptic_angle = 2*np.pi
    feedback_controller = hopif.PID(0.040, 1e-4, 1e-4)
    then = time.time()
    while True:
        state = hopper.getState()
        if state is not None:
            now = time.time()
            dt = now - then
            state_filter.insert(state)
            median_state = state_filter.getMedian()
            error = target_haptic_angle - median_state.haptic_m_angle
            print(error)
            effort = feedback_controller.update(error, dt)  + ff_effort
            hopper.setHapticMEffort(effort)
            hopper.updateMotorCommand()
            if abs(error) < 5.e-2:
                break

    # Reinitialize state and filter
    state = None
    cnt = 0
    while True:
        state = hopper.getState()
        if state is not None:
            state_filter.insert(state)
            cnt+=1
        if cnt == state_filter._window_len:
            break

    zero_position = pulley_radius * median_state.haptic_m_angle
     # Write jockey parameters to file
    line = '%.4f, %.4f, %.4f, %.4f\n' % (mass, natural_frequency, damping_ratio, zero_position)
    f.write(line)

    try:
        then = time.time()
        start_time = then
        while True:
            state = hopper.getState()
            if state is not None:
                now = time.time()
                dt = now - then
                then = now
                state_filter.insert(state)
                median_state = state_filter.getMedian()
                angular_velocity = median_state.haptic_m_v
                hopper_position = pulley_radius * median_state.haptic_m_angle
                hopper_velocity = pulley_radius * angular_velocity
                spring_damper_force =  -spring_constant*(hopper_position-zero_position)
                spring_damper_force += - drag_constant*hopper_velocity + mass * 9.81
                haptic_m_effort = hopif.force_lookup(angular_velocity
                                                    , spring_damper_force)
                hopper.setHapticMEffort(haptic_m_effort)
                hopper.updateMotorCommand()
                data = '%.4f,%.4f,%.4f,%.4f,%.4f,%.4f\n' % (hopper_position
                                                            , hopper_velocity
                                                            , spring_damper_force
                                                            , haptic_m_effort
                                                            , now-start_time
                                                            , dt
                                                            )
                f.write(data)
            if keyboard.is_pressed('enter'):
                break

        # deactivate hopper
        # try
        hopper.stopAllMotors()
        time.sleep(1)
        hopper.disconnect()

    except BaseException as e:
        print("caught exception " + str(e) + "\n Stopping program now!")
        print(type(e))
        # deactivate hopper
        # try
        hopper.stopAllMotors()
        time.sleep(1)
        hopper.disconnect()
