"""
In this file is an example of how to parse the CSV data produced by hop.py
"""
#%% Module imports
from matplotlib import pyplot as plt 
from pandas import DataFrame, read_csv
import hopper_interface as hopif
pulley_radius = 1.5 * 2.54 / 100. # needed to convert radians to meters

#%% Reading data
f = open('foo.csv', 'r')
data = [line.rstrip('\n').split(',') for line in f]
print(data[0])
#%% Parse data into pandas
columns = ['m1 angle'
            , 'm2 angle'
            , 'm3 angle'
            ,'m1 velocity'
            , 'm2 velocity'
            , 'm3 velocity'
            , 'leg length'
            ,'leg velocity'
            ,'m1 effort'
            , 'm2 effort'
            , 'm3 effort'
            ,'time'
            , 'dt']

df = DataFrame(data, columns=columns, dtype=float)

#%%
#plt.plot(df['time'], df['leg length'])
#plt.plot(df['time'], pulley_radius*df['leg velocity'])

#%%
plt.plot(df['time'], df['m1 effort'])
plt.plot(df['time'], df['m2 effort'])
plt.show()
#%% Close data files
f.close()
