import hopper_interface as hopif
import time
import numpy as np
import keyboard

def parse_state(state):
    """ Helper function for parsing state into CSV """
    line = "%.4f,%.4f,%.4f,%.4f,%.4f,%.4f" % (state.leg_m1_angle, state.leg_m2_angle, state.haptic_m_angle, state.leg_m1_v, state.leg_m2_v, state.haptic_m_v)
    return line

# Open connection to hopper
hopper = hopif.HopperInterface('enp14s0')
hopper.setHapticMEffort(0.)
hopper.updateMotorCommand()

# Initialize state filter
state_filter = hopif.StateMedianFilter(3)

# Get initial hopper state
state = None
cnt = 0
while True:
    state = hopper.getState()
    if state is not None:
        state_filter.insert(state)
        cnt+=1
    if cnt == state_filter._window_len:
        break

"""
Raise the robot to a specified height (as represented by the total angle of the haptic motor)
"""
ff_effort = .12
target_haptic_angle = 3*np.pi
feedback_controller = hopif.PID(0.04, 1e-4, 1e-4)
then = time.time()
while True:
    state = hopper.getState()
    if state is not None:
        now = time.time()
        dt = now - then
        state_filter.insert(state)
        median_state = state_filter.getMedian()
        error = target_haptic_angle - median_state.haptic_m_angle
        effort = feedback_controller.update(error, dt)  + ff_effort
        hopper.setHapticMEffort(effort)
        hopper.updateMotorCommand()
        if abs(error) < 5.e-2:
            break

# create PIDS
m1_pid = hopif.PID(.15, 0., 1e-6)
m2_pid = hopif.PID(.15, 0., 1e-6)

# Set gait parameters
m1_extend_effort = -.45
m2_extend_effort = .45
min_leg_len = hopif.percentToLength(0.3)
max_leg_len = hopif.percentToLength(0.8)
retract_sp = hopif.legCoordsToMotorAngles(max_leg_len, np.pi/2)
m1_retract_sp = retract_sp[0]
m2_retract_sp = retract_sp[1]

time.sleep(1.)

# Reinitialize state and filter
state = None
cnt = 0
while True:
    state = hopper.getState()
    if state is not None:
        state_filter.insert(state)
        cnt+=1
    if cnt == state_filter._window_len:
        break

# Open output file for data logging
filename = raw_input("Please provide filename: ")
f = open(filename, 'w')
foo = raw_input("press anything to begin")

# initialize leg position
then = time.time()
while True:
    state = hopper.getState()
    if state is not None:
        now = time.time()
        dt = now-then
        then=now

        leg_len = hopif.getLegLength(state)
        length_error = max_leg_len - leg_len
        if abs(length_error) < 0.01:
            break
        m1_error = m1_retract_sp-state.leg_m1_angle
        m2_error = m2_retract_sp-state.leg_m2_angle
        m1_effort = m1_pid.update(m1_error, dt)
        m2_effort = m2_pid.update(m2_error, dt)
        print('m1 effort %f\n m2 effort %f' %(m1_effort,m2_effort))
        hopper.setLegM1Effort(m1_effort)
        hopper.setLegM2Effort(m2_effort)
        hopper.updateMotorCommand()
m1_pid.reset()
m2_pid.reset()

# drop thebot
print("beginning")
hopper.setHapticMEffort(0.)
hopper.updateMotorCommand()

hopper_state = "retract"
try:
    then = time.time()
    start_time = then 
    while True:
        state = hopper.getState()
        if state is not None:
            now = time.time()
            dt = now - then
            then = now
            state_filter.insert(state)
            median_state = state_filter.getMedian()

            # compensate haptic motor back-emf
            angular_velocity = median_state.haptic_m_v
            force_desired = 0
            haptic_m_effort = hopif.force_lookup(angular_velocity, force_desired)
            hopper.setHapticMEffort(haptic_m_effort)

            # generate hopping behavior
            leg_len = hopif.getLegLength(median_state)
            if hopper_state == "retract":
                if leg_len <= min_leg_len:
                    print("Switching to extension state")
                    hopper_state = "extend"
                    m1_pid.reset()
                else:
                    m1_error = m1_retract_sp - median_state.leg_m1_angle
                    m2_error = m2_retract_sp - median_state.leg_m2_angle
                    m1_effort = m1_pid.update(m1_error, dt)
                    m2_effort = m2_pid.update(m2_error, dt)
                    hopper.setLegM1Effort(m1_effort)
                    hopper.setLegM2Effort(m2_effort)
            if hopper_state == "extend":
                if leg_len > max_leg_len:
                    print("Switching to retraction state")
                    hopper_state = "retract"
                    m1_pid.reset()
                    m2_pid.reset()
                else:
                    m1_effort = m1_extend_effort
                    m2_effort = m2_extend_effort
                    hopper.setLegM1Effort(m1_effort)
                    hopper.setLegM2Effort(m2_effort)
            hopper.updateMotorCommand()

            # Write motor telemetry to an output file
            data = parse_state(state)
            data += ',%.4f,%.4f' % (leg_len, hopif.getLegVelocity(state))
            data += ',%.4f,%.4f,%.4f,%.4f,%.4f\n' % (m1_effort
                                                    , m2_effort
                                                    , haptic_m_effort
                                                    , now-start_time
                                                    , dt)
            f.write(data)

        # Exit when key pressed
        if keyboard.is_pressed('enter'):
            break

    hopper.stopAllMotors()
    time.sleep(1)
    hopper.disconnect()

except BaseException as e:
    print("caught exception " + str(e) + "\n Stopping program now!")
    hopper.stopAllMotors()
    time.sleep(1)
    hopper.disconnect()
