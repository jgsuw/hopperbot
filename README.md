This repository contains the code for programming the AMP lab hopper robot. Currently the code is being maintained and actively developed by Joseph Sullivan. The codebase has three main elements 

- The Ghost Robotics SDK, this provides the embedded software layer for control
- The Main-Board code, an embedded application that implements the hardware end of the control API
- The Hopper Interface, a python module that implements the user-facing software end of the control API

The robot can be controlled in closed-loop via an application on an external computer, using the Hopper Interface python module. To do this, there must be a direct wired ethernet connection between the computer which runs the control application and the GhostRobotics main-board. This network interface must be configured with the static IP address `169.254.98.1`. To use this code, refer to the HopperInterface and MotorTelemetry classes in the hopper_interface module, and to the example code.

# Installation
In order to use the Hopper Interface, the user must install the module located in hopper_interface so that it is in their python environment. For example, using pip one would `$ cd hopper_interface; pip install .` to install the module. Using Anaconda, one would do `$ cd hopper_interface; conda develop .`
