/** @file
 *	@brief MAVLink comm testsuite protocol generated from robottelemetry.xml
 *	@see http://mavlink.org
 */

#pragma once

#include <gtest/gtest.h>
#include "robottelemetry.hpp"

#ifdef TEST_INTEROP
using namespace mavlink;
#undef MAVLINK_HELPER
#include "mavlink.h"
#endif


TEST(robottelemetry, HEARTBEAT)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::HEARTBEAT packet_in{};
    packet_in.type = 17;
    packet_in.autopilot = 84;
    packet_in.base_mode = 151;
    packet_in.custom_mode = 963497464;
    packet_in.system_status = 218;
    packet_in.mavlink_version = 3;

    mavlink::robottelemetry::msg::HEARTBEAT packet1{};
    mavlink::robottelemetry::msg::HEARTBEAT packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.type, packet2.type);
    EXPECT_EQ(packet1.autopilot, packet2.autopilot);
    EXPECT_EQ(packet1.base_mode, packet2.base_mode);
    EXPECT_EQ(packet1.custom_mode, packet2.custom_mode);
    EXPECT_EQ(packet1.system_status, packet2.system_status);
    EXPECT_EQ(packet1.mavlink_version, packet2.mavlink_version);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, HEARTBEAT)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_heartbeat_t packet_c {
         963497464, 17, 84, 151, 218, 3
    };

    mavlink::robottelemetry::msg::HEARTBEAT packet_in{};
    packet_in.type = 17;
    packet_in.autopilot = 84;
    packet_in.base_mode = 151;
    packet_in.custom_mode = 963497464;
    packet_in.system_status = 218;
    packet_in.mavlink_version = 3;

    mavlink::robottelemetry::msg::HEARTBEAT packet2{};

    mavlink_msg_heartbeat_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.type, packet2.type);
    EXPECT_EQ(packet_in.autopilot, packet2.autopilot);
    EXPECT_EQ(packet_in.base_mode, packet2.base_mode);
    EXPECT_EQ(packet_in.custom_mode, packet2.custom_mode);
    EXPECT_EQ(packet_in.system_status, packet2.system_status);
    EXPECT_EQ(packet_in.mavlink_version, packet2.mavlink_version);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, SET_MODE)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::SET_MODE packet_in{};
    packet_in.target_system = 17;
    packet_in.base_mode = 84;
    packet_in.custom_mode = 963497464;

    mavlink::robottelemetry::msg::SET_MODE packet1{};
    mavlink::robottelemetry::msg::SET_MODE packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.target_system, packet2.target_system);
    EXPECT_EQ(packet1.base_mode, packet2.base_mode);
    EXPECT_EQ(packet1.custom_mode, packet2.custom_mode);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, SET_MODE)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_set_mode_t packet_c {
         963497464, 17, 84
    };

    mavlink::robottelemetry::msg::SET_MODE packet_in{};
    packet_in.target_system = 17;
    packet_in.base_mode = 84;
    packet_in.custom_mode = 963497464;

    mavlink::robottelemetry::msg::SET_MODE packet2{};

    mavlink_msg_set_mode_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.target_system, packet2.target_system);
    EXPECT_EQ(packet_in.base_mode, packet2.base_mode);
    EXPECT_EQ(packet_in.custom_mode, packet2.custom_mode);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, BATTERY_STATUS)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::BATTERY_STATUS packet_in{};
    packet_in.id = 101;
    packet_in.battery_function = 168;
    packet_in.type = 235;
    packet_in.temperature = 17651;
    packet_in.voltages = {{ 17755, 17756, 17757, 17758, 17759, 17760, 17761, 17762, 17763, 17764 }};
    packet_in.current_battery = 18795;
    packet_in.current_consumed = 963497464;
    packet_in.energy_consumed = 963497672;
    packet_in.battery_remaining = 46;

    mavlink::robottelemetry::msg::BATTERY_STATUS packet1{};
    mavlink::robottelemetry::msg::BATTERY_STATUS packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.id, packet2.id);
    EXPECT_EQ(packet1.battery_function, packet2.battery_function);
    EXPECT_EQ(packet1.type, packet2.type);
    EXPECT_EQ(packet1.temperature, packet2.temperature);
    EXPECT_EQ(packet1.voltages, packet2.voltages);
    EXPECT_EQ(packet1.current_battery, packet2.current_battery);
    EXPECT_EQ(packet1.current_consumed, packet2.current_consumed);
    EXPECT_EQ(packet1.energy_consumed, packet2.energy_consumed);
    EXPECT_EQ(packet1.battery_remaining, packet2.battery_remaining);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, BATTERY_STATUS)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_battery_status_t packet_c {
         963497464, 963497672, 17651, { 17755, 17756, 17757, 17758, 17759, 17760, 17761, 17762, 17763, 17764 }, 18795, 101, 168, 235, 46
    };

    mavlink::robottelemetry::msg::BATTERY_STATUS packet_in{};
    packet_in.id = 101;
    packet_in.battery_function = 168;
    packet_in.type = 235;
    packet_in.temperature = 17651;
    packet_in.voltages = {{ 17755, 17756, 17757, 17758, 17759, 17760, 17761, 17762, 17763, 17764 }};
    packet_in.current_battery = 18795;
    packet_in.current_consumed = 963497464;
    packet_in.energy_consumed = 963497672;
    packet_in.battery_remaining = 46;

    mavlink::robottelemetry::msg::BATTERY_STATUS packet2{};

    mavlink_msg_battery_status_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.id, packet2.id);
    EXPECT_EQ(packet_in.battery_function, packet2.battery_function);
    EXPECT_EQ(packet_in.type, packet2.type);
    EXPECT_EQ(packet_in.temperature, packet2.temperature);
    EXPECT_EQ(packet_in.voltages, packet2.voltages);
    EXPECT_EQ(packet_in.current_battery, packet2.current_battery);
    EXPECT_EQ(packet_in.current_consumed, packet2.current_consumed);
    EXPECT_EQ(packet_in.energy_consumed, packet2.energy_consumed);
    EXPECT_EQ(packet_in.battery_remaining, packet2.battery_remaining);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, SCALED_IMU)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::SCALED_IMU packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.xacc = 17443;
    packet_in.yacc = 17547;
    packet_in.zacc = 17651;
    packet_in.xgyro = 17755;
    packet_in.ygyro = 17859;
    packet_in.zgyro = 17963;
    packet_in.xmag = 18067;
    packet_in.ymag = 18171;
    packet_in.zmag = 18275;

    mavlink::robottelemetry::msg::SCALED_IMU packet1{};
    mavlink::robottelemetry::msg::SCALED_IMU packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet1.xacc, packet2.xacc);
    EXPECT_EQ(packet1.yacc, packet2.yacc);
    EXPECT_EQ(packet1.zacc, packet2.zacc);
    EXPECT_EQ(packet1.xgyro, packet2.xgyro);
    EXPECT_EQ(packet1.ygyro, packet2.ygyro);
    EXPECT_EQ(packet1.zgyro, packet2.zgyro);
    EXPECT_EQ(packet1.xmag, packet2.xmag);
    EXPECT_EQ(packet1.ymag, packet2.ymag);
    EXPECT_EQ(packet1.zmag, packet2.zmag);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, SCALED_IMU)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_scaled_imu_t packet_c {
         963497464, 17443, 17547, 17651, 17755, 17859, 17963, 18067, 18171, 18275
    };

    mavlink::robottelemetry::msg::SCALED_IMU packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.xacc = 17443;
    packet_in.yacc = 17547;
    packet_in.zacc = 17651;
    packet_in.xgyro = 17755;
    packet_in.ygyro = 17859;
    packet_in.zgyro = 17963;
    packet_in.xmag = 18067;
    packet_in.ymag = 18171;
    packet_in.zmag = 18275;

    mavlink::robottelemetry::msg::SCALED_IMU packet2{};

    mavlink_msg_scaled_imu_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet_in.xacc, packet2.xacc);
    EXPECT_EQ(packet_in.yacc, packet2.yacc);
    EXPECT_EQ(packet_in.zacc, packet2.zacc);
    EXPECT_EQ(packet_in.xgyro, packet2.xgyro);
    EXPECT_EQ(packet_in.ygyro, packet2.ygyro);
    EXPECT_EQ(packet_in.zgyro, packet2.zgyro);
    EXPECT_EQ(packet_in.xmag, packet2.xmag);
    EXPECT_EQ(packet_in.ymag, packet2.ymag);
    EXPECT_EQ(packet_in.zmag, packet2.zmag);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, ATTITUDE_QUATERNION)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::ATTITUDE_QUATERNION packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.q1 = 45.0;
    packet_in.q2 = 73.0;
    packet_in.q3 = 101.0;
    packet_in.q4 = 129.0;
    packet_in.rollspeed = 157.0;
    packet_in.pitchspeed = 185.0;
    packet_in.yawspeed = 213.0;

    mavlink::robottelemetry::msg::ATTITUDE_QUATERNION packet1{};
    mavlink::robottelemetry::msg::ATTITUDE_QUATERNION packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet1.q1, packet2.q1);
    EXPECT_EQ(packet1.q2, packet2.q2);
    EXPECT_EQ(packet1.q3, packet2.q3);
    EXPECT_EQ(packet1.q4, packet2.q4);
    EXPECT_EQ(packet1.rollspeed, packet2.rollspeed);
    EXPECT_EQ(packet1.pitchspeed, packet2.pitchspeed);
    EXPECT_EQ(packet1.yawspeed, packet2.yawspeed);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, ATTITUDE_QUATERNION)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_attitude_quaternion_t packet_c {
         963497464, 45.0, 73.0, 101.0, 129.0, 157.0, 185.0, 213.0
    };

    mavlink::robottelemetry::msg::ATTITUDE_QUATERNION packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.q1 = 45.0;
    packet_in.q2 = 73.0;
    packet_in.q3 = 101.0;
    packet_in.q4 = 129.0;
    packet_in.rollspeed = 157.0;
    packet_in.pitchspeed = 185.0;
    packet_in.yawspeed = 213.0;

    mavlink::robottelemetry::msg::ATTITUDE_QUATERNION packet2{};

    mavlink_msg_attitude_quaternion_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet_in.q1, packet2.q1);
    EXPECT_EQ(packet_in.q2, packet2.q2);
    EXPECT_EQ(packet_in.q3, packet2.q3);
    EXPECT_EQ(packet_in.q4, packet2.q4);
    EXPECT_EQ(packet_in.rollspeed, packet2.rollspeed);
    EXPECT_EQ(packet_in.pitchspeed, packet2.pitchspeed);
    EXPECT_EQ(packet_in.yawspeed, packet2.yawspeed);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, LOCAL_POSITION_NED)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::LOCAL_POSITION_NED packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.x = 45.0;
    packet_in.y = 73.0;
    packet_in.z = 101.0;
    packet_in.vx = 129.0;
    packet_in.vy = 157.0;
    packet_in.vz = 185.0;

    mavlink::robottelemetry::msg::LOCAL_POSITION_NED packet1{};
    mavlink::robottelemetry::msg::LOCAL_POSITION_NED packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet1.x, packet2.x);
    EXPECT_EQ(packet1.y, packet2.y);
    EXPECT_EQ(packet1.z, packet2.z);
    EXPECT_EQ(packet1.vx, packet2.vx);
    EXPECT_EQ(packet1.vy, packet2.vy);
    EXPECT_EQ(packet1.vz, packet2.vz);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, LOCAL_POSITION_NED)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_local_position_ned_t packet_c {
         963497464, 45.0, 73.0, 101.0, 129.0, 157.0, 185.0
    };

    mavlink::robottelemetry::msg::LOCAL_POSITION_NED packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.x = 45.0;
    packet_in.y = 73.0;
    packet_in.z = 101.0;
    packet_in.vx = 129.0;
    packet_in.vy = 157.0;
    packet_in.vz = 185.0;

    mavlink::robottelemetry::msg::LOCAL_POSITION_NED packet2{};

    mavlink_msg_local_position_ned_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet_in.x, packet2.x);
    EXPECT_EQ(packet_in.y, packet2.y);
    EXPECT_EQ(packet_in.z, packet2.z);
    EXPECT_EQ(packet_in.vx, packet2.vx);
    EXPECT_EQ(packet_in.vy, packet2.vy);
    EXPECT_EQ(packet_in.vz, packet2.vz);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, GLOBAL_POSITION_INT)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::GLOBAL_POSITION_INT packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.lat = 963497672;
    packet_in.lon = 963497880;
    packet_in.alt = 963498088;
    packet_in.relative_alt = 963498296;
    packet_in.vx = 18275;
    packet_in.vy = 18379;
    packet_in.vz = 18483;
    packet_in.hdg = 18587;

    mavlink::robottelemetry::msg::GLOBAL_POSITION_INT packet1{};
    mavlink::robottelemetry::msg::GLOBAL_POSITION_INT packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet1.lat, packet2.lat);
    EXPECT_EQ(packet1.lon, packet2.lon);
    EXPECT_EQ(packet1.alt, packet2.alt);
    EXPECT_EQ(packet1.relative_alt, packet2.relative_alt);
    EXPECT_EQ(packet1.vx, packet2.vx);
    EXPECT_EQ(packet1.vy, packet2.vy);
    EXPECT_EQ(packet1.vz, packet2.vz);
    EXPECT_EQ(packet1.hdg, packet2.hdg);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, GLOBAL_POSITION_INT)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_global_position_int_t packet_c {
         963497464, 963497672, 963497880, 963498088, 963498296, 18275, 18379, 18483, 18587
    };

    mavlink::robottelemetry::msg::GLOBAL_POSITION_INT packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.lat = 963497672;
    packet_in.lon = 963497880;
    packet_in.alt = 963498088;
    packet_in.relative_alt = 963498296;
    packet_in.vx = 18275;
    packet_in.vy = 18379;
    packet_in.vz = 18483;
    packet_in.hdg = 18587;

    mavlink::robottelemetry::msg::GLOBAL_POSITION_INT packet2{};

    mavlink_msg_global_position_int_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet_in.lat, packet2.lat);
    EXPECT_EQ(packet_in.lon, packet2.lon);
    EXPECT_EQ(packet_in.alt, packet2.alt);
    EXPECT_EQ(packet_in.relative_alt, packet2.relative_alt);
    EXPECT_EQ(packet_in.vx, packet2.vx);
    EXPECT_EQ(packet_in.vy, packet2.vy);
    EXPECT_EQ(packet_in.vz, packet2.vz);
    EXPECT_EQ(packet_in.hdg, packet2.hdg);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, SET_POSITION_TARGET_LOCAL_NED)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::SET_POSITION_TARGET_LOCAL_NED packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.target_system = 27;
    packet_in.target_component = 94;
    packet_in.coordinate_frame = 161;
    packet_in.type_mask = 19731;
    packet_in.x = 45.0;
    packet_in.y = 73.0;
    packet_in.z = 101.0;
    packet_in.vx = 129.0;
    packet_in.vy = 157.0;
    packet_in.vz = 185.0;
    packet_in.afx = 213.0;
    packet_in.afy = 241.0;
    packet_in.afz = 269.0;
    packet_in.yaw = 297.0;
    packet_in.yaw_rate = 325.0;

    mavlink::robottelemetry::msg::SET_POSITION_TARGET_LOCAL_NED packet1{};
    mavlink::robottelemetry::msg::SET_POSITION_TARGET_LOCAL_NED packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet1.target_system, packet2.target_system);
    EXPECT_EQ(packet1.target_component, packet2.target_component);
    EXPECT_EQ(packet1.coordinate_frame, packet2.coordinate_frame);
    EXPECT_EQ(packet1.type_mask, packet2.type_mask);
    EXPECT_EQ(packet1.x, packet2.x);
    EXPECT_EQ(packet1.y, packet2.y);
    EXPECT_EQ(packet1.z, packet2.z);
    EXPECT_EQ(packet1.vx, packet2.vx);
    EXPECT_EQ(packet1.vy, packet2.vy);
    EXPECT_EQ(packet1.vz, packet2.vz);
    EXPECT_EQ(packet1.afx, packet2.afx);
    EXPECT_EQ(packet1.afy, packet2.afy);
    EXPECT_EQ(packet1.afz, packet2.afz);
    EXPECT_EQ(packet1.yaw, packet2.yaw);
    EXPECT_EQ(packet1.yaw_rate, packet2.yaw_rate);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, SET_POSITION_TARGET_LOCAL_NED)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_set_position_target_local_ned_t packet_c {
         963497464, 45.0, 73.0, 101.0, 129.0, 157.0, 185.0, 213.0, 241.0, 269.0, 297.0, 325.0, 19731, 27, 94, 161
    };

    mavlink::robottelemetry::msg::SET_POSITION_TARGET_LOCAL_NED packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.target_system = 27;
    packet_in.target_component = 94;
    packet_in.coordinate_frame = 161;
    packet_in.type_mask = 19731;
    packet_in.x = 45.0;
    packet_in.y = 73.0;
    packet_in.z = 101.0;
    packet_in.vx = 129.0;
    packet_in.vy = 157.0;
    packet_in.vz = 185.0;
    packet_in.afx = 213.0;
    packet_in.afy = 241.0;
    packet_in.afz = 269.0;
    packet_in.yaw = 297.0;
    packet_in.yaw_rate = 325.0;

    mavlink::robottelemetry::msg::SET_POSITION_TARGET_LOCAL_NED packet2{};

    mavlink_msg_set_position_target_local_ned_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet_in.target_system, packet2.target_system);
    EXPECT_EQ(packet_in.target_component, packet2.target_component);
    EXPECT_EQ(packet_in.coordinate_frame, packet2.coordinate_frame);
    EXPECT_EQ(packet_in.type_mask, packet2.type_mask);
    EXPECT_EQ(packet_in.x, packet2.x);
    EXPECT_EQ(packet_in.y, packet2.y);
    EXPECT_EQ(packet_in.z, packet2.z);
    EXPECT_EQ(packet_in.vx, packet2.vx);
    EXPECT_EQ(packet_in.vy, packet2.vy);
    EXPECT_EQ(packet_in.vz, packet2.vz);
    EXPECT_EQ(packet_in.afx, packet2.afx);
    EXPECT_EQ(packet_in.afy, packet2.afy);
    EXPECT_EQ(packet_in.afz, packet2.afz);
    EXPECT_EQ(packet_in.yaw, packet2.yaw);
    EXPECT_EQ(packet_in.yaw_rate, packet2.yaw_rate);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, POSITION_TARGET_LOCAL_NED)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::POSITION_TARGET_LOCAL_NED packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.coordinate_frame = 27;
    packet_in.type_mask = 19731;
    packet_in.x = 45.0;
    packet_in.y = 73.0;
    packet_in.z = 101.0;
    packet_in.vx = 129.0;
    packet_in.vy = 157.0;
    packet_in.vz = 185.0;
    packet_in.afx = 213.0;
    packet_in.afy = 241.0;
    packet_in.afz = 269.0;
    packet_in.yaw = 297.0;
    packet_in.yaw_rate = 325.0;

    mavlink::robottelemetry::msg::POSITION_TARGET_LOCAL_NED packet1{};
    mavlink::robottelemetry::msg::POSITION_TARGET_LOCAL_NED packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet1.coordinate_frame, packet2.coordinate_frame);
    EXPECT_EQ(packet1.type_mask, packet2.type_mask);
    EXPECT_EQ(packet1.x, packet2.x);
    EXPECT_EQ(packet1.y, packet2.y);
    EXPECT_EQ(packet1.z, packet2.z);
    EXPECT_EQ(packet1.vx, packet2.vx);
    EXPECT_EQ(packet1.vy, packet2.vy);
    EXPECT_EQ(packet1.vz, packet2.vz);
    EXPECT_EQ(packet1.afx, packet2.afx);
    EXPECT_EQ(packet1.afy, packet2.afy);
    EXPECT_EQ(packet1.afz, packet2.afz);
    EXPECT_EQ(packet1.yaw, packet2.yaw);
    EXPECT_EQ(packet1.yaw_rate, packet2.yaw_rate);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, POSITION_TARGET_LOCAL_NED)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_position_target_local_ned_t packet_c {
         963497464, 45.0, 73.0, 101.0, 129.0, 157.0, 185.0, 213.0, 241.0, 269.0, 297.0, 325.0, 19731, 27
    };

    mavlink::robottelemetry::msg::POSITION_TARGET_LOCAL_NED packet_in{};
    packet_in.time_boot_ms = 963497464;
    packet_in.coordinate_frame = 27;
    packet_in.type_mask = 19731;
    packet_in.x = 45.0;
    packet_in.y = 73.0;
    packet_in.z = 101.0;
    packet_in.vx = 129.0;
    packet_in.vy = 157.0;
    packet_in.vz = 185.0;
    packet_in.afx = 213.0;
    packet_in.afy = 241.0;
    packet_in.afz = 269.0;
    packet_in.yaw = 297.0;
    packet_in.yaw_rate = 325.0;

    mavlink::robottelemetry::msg::POSITION_TARGET_LOCAL_NED packet2{};

    mavlink_msg_position_target_local_ned_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.time_boot_ms, packet2.time_boot_ms);
    EXPECT_EQ(packet_in.coordinate_frame, packet2.coordinate_frame);
    EXPECT_EQ(packet_in.type_mask, packet2.type_mask);
    EXPECT_EQ(packet_in.x, packet2.x);
    EXPECT_EQ(packet_in.y, packet2.y);
    EXPECT_EQ(packet_in.z, packet2.z);
    EXPECT_EQ(packet_in.vx, packet2.vx);
    EXPECT_EQ(packet_in.vy, packet2.vy);
    EXPECT_EQ(packet_in.vz, packet2.vz);
    EXPECT_EQ(packet_in.afx, packet2.afx);
    EXPECT_EQ(packet_in.afy, packet2.afy);
    EXPECT_EQ(packet_in.afz, packet2.afz);
    EXPECT_EQ(packet_in.yaw, packet2.yaw);
    EXPECT_EQ(packet_in.yaw_rate, packet2.yaw_rate);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif

TEST(robottelemetry, SERVO_OUTPUT_RAW)
{
    mavlink::mavlink_message_t msg;
    mavlink::MsgMap map1(msg);
    mavlink::MsgMap map2(msg);

    mavlink::robottelemetry::msg::SERVO_OUTPUT_RAW packet_in{};
    packet_in.time_usec = 963497464;
    packet_in.port = 65;
    packet_in.servo1_raw = 17443;
    packet_in.servo2_raw = 17547;
    packet_in.servo3_raw = 17651;
    packet_in.servo4_raw = 17755;
    packet_in.servo5_raw = 17859;
    packet_in.servo6_raw = 17963;
    packet_in.servo7_raw = 18067;
    packet_in.servo8_raw = 18171;
    packet_in.servo9_raw = 18327;
    packet_in.servo10_raw = 18431;
    packet_in.servo11_raw = 18535;
    packet_in.servo12_raw = 18639;
    packet_in.servo13_raw = 18743;
    packet_in.servo14_raw = 18847;
    packet_in.servo15_raw = 18951;
    packet_in.servo16_raw = 19055;

    mavlink::robottelemetry::msg::SERVO_OUTPUT_RAW packet1{};
    mavlink::robottelemetry::msg::SERVO_OUTPUT_RAW packet2{};

    packet1 = packet_in;

    //std::cout << packet1.to_yaml() << std::endl;

    packet1.serialize(map1);

    mavlink::mavlink_finalize_message(&msg, 1, 1, packet1.MIN_LENGTH, packet1.LENGTH, packet1.CRC_EXTRA);

    packet2.deserialize(map2);

    EXPECT_EQ(packet1.time_usec, packet2.time_usec);
    EXPECT_EQ(packet1.port, packet2.port);
    EXPECT_EQ(packet1.servo1_raw, packet2.servo1_raw);
    EXPECT_EQ(packet1.servo2_raw, packet2.servo2_raw);
    EXPECT_EQ(packet1.servo3_raw, packet2.servo3_raw);
    EXPECT_EQ(packet1.servo4_raw, packet2.servo4_raw);
    EXPECT_EQ(packet1.servo5_raw, packet2.servo5_raw);
    EXPECT_EQ(packet1.servo6_raw, packet2.servo6_raw);
    EXPECT_EQ(packet1.servo7_raw, packet2.servo7_raw);
    EXPECT_EQ(packet1.servo8_raw, packet2.servo8_raw);
    EXPECT_EQ(packet1.servo9_raw, packet2.servo9_raw);
    EXPECT_EQ(packet1.servo10_raw, packet2.servo10_raw);
    EXPECT_EQ(packet1.servo11_raw, packet2.servo11_raw);
    EXPECT_EQ(packet1.servo12_raw, packet2.servo12_raw);
    EXPECT_EQ(packet1.servo13_raw, packet2.servo13_raw);
    EXPECT_EQ(packet1.servo14_raw, packet2.servo14_raw);
    EXPECT_EQ(packet1.servo15_raw, packet2.servo15_raw);
    EXPECT_EQ(packet1.servo16_raw, packet2.servo16_raw);
}

#ifdef TEST_INTEROP
TEST(robottelemetry_interop, SERVO_OUTPUT_RAW)
{
    mavlink_message_t msg;

    // to get nice print
    memset(&msg, 0, sizeof(msg));

    mavlink_servo_output_raw_t packet_c {
         963497464, 17443, 17547, 17651, 17755, 17859, 17963, 18067, 18171, 65, 18327, 18431, 18535, 18639, 18743, 18847, 18951, 19055
    };

    mavlink::robottelemetry::msg::SERVO_OUTPUT_RAW packet_in{};
    packet_in.time_usec = 963497464;
    packet_in.port = 65;
    packet_in.servo1_raw = 17443;
    packet_in.servo2_raw = 17547;
    packet_in.servo3_raw = 17651;
    packet_in.servo4_raw = 17755;
    packet_in.servo5_raw = 17859;
    packet_in.servo6_raw = 17963;
    packet_in.servo7_raw = 18067;
    packet_in.servo8_raw = 18171;
    packet_in.servo9_raw = 18327;
    packet_in.servo10_raw = 18431;
    packet_in.servo11_raw = 18535;
    packet_in.servo12_raw = 18639;
    packet_in.servo13_raw = 18743;
    packet_in.servo14_raw = 18847;
    packet_in.servo15_raw = 18951;
    packet_in.servo16_raw = 19055;

    mavlink::robottelemetry::msg::SERVO_OUTPUT_RAW packet2{};

    mavlink_msg_servo_output_raw_encode(1, 1, &msg, &packet_c);

    // simulate message-handling callback
    [&packet2](const mavlink_message_t *cmsg) {
        MsgMap map2(cmsg);

        packet2.deserialize(map2);
    } (&msg);

    EXPECT_EQ(packet_in.time_usec, packet2.time_usec);
    EXPECT_EQ(packet_in.port, packet2.port);
    EXPECT_EQ(packet_in.servo1_raw, packet2.servo1_raw);
    EXPECT_EQ(packet_in.servo2_raw, packet2.servo2_raw);
    EXPECT_EQ(packet_in.servo3_raw, packet2.servo3_raw);
    EXPECT_EQ(packet_in.servo4_raw, packet2.servo4_raw);
    EXPECT_EQ(packet_in.servo5_raw, packet2.servo5_raw);
    EXPECT_EQ(packet_in.servo6_raw, packet2.servo6_raw);
    EXPECT_EQ(packet_in.servo7_raw, packet2.servo7_raw);
    EXPECT_EQ(packet_in.servo8_raw, packet2.servo8_raw);
    EXPECT_EQ(packet_in.servo9_raw, packet2.servo9_raw);
    EXPECT_EQ(packet_in.servo10_raw, packet2.servo10_raw);
    EXPECT_EQ(packet_in.servo11_raw, packet2.servo11_raw);
    EXPECT_EQ(packet_in.servo12_raw, packet2.servo12_raw);
    EXPECT_EQ(packet_in.servo13_raw, packet2.servo13_raw);
    EXPECT_EQ(packet_in.servo14_raw, packet2.servo14_raw);
    EXPECT_EQ(packet_in.servo15_raw, packet2.servo15_raw);
    EXPECT_EQ(packet_in.servo16_raw, packet2.servo16_raw);

#ifdef PRINT_MSG
    PRINT_MSG(msg);
#endif
}
#endif
