python pymavlink/tools/mavgen.py robotcontrol.xml --lang Java  --wire-protocol=2.0 -o robotcontrol_java
python pymavlink/tools/mavgen.py robotcontrol.xml --lang C++11 --wire-protocol=2.0 -o robotcontrol_cpp
python pymavlink/tools/mavgen.py robottelemetry.xml --lang Java --wire-protocol=2.0 -o robottelemetry_java
python pymavlink/tools/mavgen.py robottelemetry.xml --lang C++11 --wire-protocol=2.0 -o robottelemetry_cpp

