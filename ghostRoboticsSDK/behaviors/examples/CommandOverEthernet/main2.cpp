/**
 * Copyright (C) Ghost Robotics - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avik De <avik@ghostrobotics.io> and Tom Jacobs <tom.jacobs@ghostrobotics.io>
 */
#include <stdio.h>
#include <SDK.h>
#include <Motor.h>

const int M[] = {6, 7}; //REQUIRED!!!!!

enum hopper_state {
 	extend,
 	contract,
}; 


hopper_state hs =contract;

class Test : public Behavior
{
// 	float stage_init = joint[M[2]].getPosition();
// 	float cur_pos;
// 	unsigned int time_a;
// 	unsigned int time_b;
// 	bool settime = true;
// 	float Kp = 1.2;
// 	float init_time = S->millis;
// 	int a;

	
public:
	void begin()
	{
		hs = contract;
	}

	void extend_fun(){
		for(int i = 0; i < 2; i++){
			joint[M[i]].setOpenLoop(0.6);
		}	
		if (joint[M[0]].getPosition() > 2.4){
				hs = contract;
			}
	}

	void contract_fun(){
		float leg_one_pos =2;
		
		for(int i = 0; i < 2; i++){
			joint[M[i]].setGain(0.45, 0.01);
			joint[M[i]].setPosition(leg_one_pos);
		}

		if (joint[M[0]].getPosition() < 1.){
				hs = extend;
		}		
	}
	
	void update()
	{
		// Control robot by joints

		C->mode = RobotCommand_Mode_JOINT; //REQUIRED!! (sort of)

		switch (hs){
				case extend:
				extend_fun();
					break;
				case contract:
				contract_fun();
					break;
		}
		
	}


		
	void end()
	{

	}
};

void cd()
{

}



/*************************************************************/
void debug() {
	for (int i = 0; i < 8; ++i)
	{
		printf("%.4f ",joint[i].getPosition());
		//printf("%.4f ",joint[i].getRawPosition());
	}
	printf("\n");
}

/*************************************************************/

int main(int argc, char *argv[])
{

	//The following lines are REQUIRED!!!

	// Loads some default settings including motor parameters
#if defined(ROBOT_MINITAUR)
	init(RobotParams_Type_MINITAUR, argc, argv);
#elif defined(ROBOT_MINITAUR_E)
	init(RobotParams_Type_MINITAUR_E, argc, argv);
#else
#error "Define robot type in preprocessor"
#endif

	// Configure joints
	#define NUM_MOTORS 8
	const float zeros[NUM_MOTORS] = {0, 0, 0, 0, 0, 0, 2.99, 2.5};
	const float directions[NUM_MOTORS] = {-1, -1, -1, -1, -1, -1, -1, -1};
	P->joints_count = S->joints_count = C->joints_count = NUM_MOTORS;
	for (int i = 0; i < P->joints_count; i++)
	{
		// Set zeros and directions
		P->joints[i].zero = zeros[i];
		P->joints[i].direction = directions[i];
		P->joints[i].gearRatio = 1;
	}

	// No limbs, only 1DOF joints
	P->limbs_count = 0;
	safetyShutoffEnable(false);

	// Remove default behaviors from behaviors vector, create, add, and start ours

	//ADD YOUR BEHAVIOR HERE!!!!
	Test test;
	behaviors.clear();
	behaviors.push_back(&test);
	test.begin();
	setDebugRate(10);

	return begin();
}
