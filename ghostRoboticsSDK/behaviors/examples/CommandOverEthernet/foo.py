import sys
import ethernet as eth
import struct
import rospy
import socket
from std_msgs.msg import Float32

class MotorTelemetry:
    def __init__(self, bstr):
        data = struct.unpack('<2f', bstr)
        self.angle = data[0]
        self.speed = data[1]

class OpenLoopMotorCommand:
    command_code = 1
    def __init__(self, idx, effort):
        self._idx = idx
        self._effort = effort
        self._raw_data = struct.pack('<Bf', command_code, effort)

    def packed(self):
        return self._raw_data

class SetMotorPosition:
    command_code = 2
    def __init__(self, idx, angle_radians):
        self._idx = idx
        self._angle_radians = angle_radians
        self._raw_data = struct.pack('<Bf', command_code, angle_radians)

    def packed(self):
        return self._raw_data

def receivePacket(srx):
    """
    Modified from the Ghost Robotics ethernet library
    """
	# RX. Keep receiving until it throws an exception, and take the freshest packet received.
	data = None
	addr = [0, 0]
	while True:
		try:
			data_received, addr_received = srx.recvfrom(eth.GRM_MAX_LENGTH)
		except:
			break
		data = data_received
		addr = addr_received

	if addr[1] != eth.MCU_SRC_PORT:
		return None, None

	try:
		magic, version, totalSize, headerCrc, numDoF, configBits = struct.unpack(eth.headerFmt, data[:eth.headerSize])

		if magic != eth.GRM_MAGIC:
			print('Magic byte did not match')
			return None, None

		# Check crc: it is contained in the header, and a crc32 of all the bytes after the header
		if headerCrc != eth.grmCrc(data[eth.headerSize: eth.headerSize + totalSize]):
			print('CRC did not match')
			return None, None

        # TODO: ought to have a proper "parsing" function
        state = struct.unpack('<9f', data[eth.headerSize:eth.headerSize+totalSize])
	except:
		return None, None
	return state, numDoF


if __name__ == "__main__":
    # Initialize ROS objects
    rospy.init_node("hopper_interface")
    m1_angle_pub = rospy.publisher("~/motor1/angle", Float32)
    m2_angle_pub = rospy.publisher("~/motor2/angle", Float32)
    m3_angle_pub = rospy.publisher("~/motor3/angle", Float32)
    m1_speed_pub = rospy.publisher("~/motor1/speed", Float32)
    m2_speed_pub = rospy.publisher("~/motor2/speed", Float32)
    m3_speed_pub = rospy.publisher("~/motor3/speed", Float32)

    # Try to open GRB ethernet interface
    iface = 'eth0' # Perhaps this could be a ROS parameter, such as from a launch file
    try:
        rx_socket = eth.openRXSocket()
        tx_socket = eth.openTXSocket(iface)
    except:
        print("Failed to open RX socket. Exiting now!")
        rospy.shutdown()
        sys.exit(0)
