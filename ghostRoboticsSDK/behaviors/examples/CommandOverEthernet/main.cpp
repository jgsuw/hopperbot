/*
MIT License (modified)

Copyright (c) 2018 Ghost Robotics
Author: Avik De <avik@ghostrobotics.io> and Tom Jacobs <tom.jacobs@ghostrobotics.io>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this **file** (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include <stdio.h>
#include <SDK.h>
#include <Motor.h>
#include <MAVCmd.h>
#include <unistd.h>
#include <ReorientableBehavior.h>
using namespace std;

#pragma pack(push, 1)
// Custom data to append to ethernet packet. No more than GRM_USER_DATA_SIZE=32 bytes!
struct UserData
{
	uint8_t bytes[GRM_USER_DATA_SIZE];
};
#pragma pack(pop)

uint32_t ethRxUpdated;
bool prevJoyRCEnabled = false;
bool joyRCEnabled = true;

class Default : public ReorientableBehavior
{
public:
	void begin()
	{
	}

	// Our received behaviorCmd and user data
	BehaviorCmd behaviorCmd;
	UserData data;

	void update()
	{
		// If no data, stop the robot
		if(S->millis > ethRxUpdated + 2000) // If it's too long past last time we received a packet
		{
			// Stop and re-enable the joystick
			if(!joyRCEnabled)
			{
				C->behavior.mode = 0;
				C->behavior.twist.linear.x = 0;
				joyRCEnabled = true;
			}
		}
		else
		{
			// Read BehaviorCmd from ethernet
			int numRead = read(ETH_UPSTREAM_FILENO, &behaviorCmd, sizeof(BehaviorCmd));
			if(numRead == sizeof(BehaviorCmd))
			{
				// If sensible command
				if(behaviorCmd.id < 10 && behaviorCmd.mode < 10 && 
				   behaviorCmd.twist.linear.x <= 1.0 && behaviorCmd.twist.linear.x >= -1.0 &&
				   behaviorCmd.twist.angular.z <= 1.0 && behaviorCmd.twist.angular.z >= -1.0)
				{
					// Copy to behavior
					memcpy(&C->behavior, &behaviorCmd, sizeof(BehaviorCmd));

					// Disable RC joystick
					joyRCEnabled = false;
				}
			}
		}

		// Disable or enable joystick once
		if(joyRCEnabled && !prevJoyRCEnabled)
		{
			// Enable joystick input
			JoyType joyType = JoyType_FRSKY_XSR;
			ioctl(JOYSTICK_FILENO, IOCTL_CMD_JOYSTICK_SET_TYPE, &joyType);
			printf("Enabling RC Joystick\n");
			C->mode = RobotCommand_Mode_LIMB;
		}
		if(!joyRCEnabled && prevJoyRCEnabled)
		{
			// Disable joystick input
			JoyType joyType = JoyType_NONE;
			ioctl(JOYSTICK_FILENO, IOCTL_CMD_JOYSTICK_SET_TYPE, &joyType);
			printf("Disabling RC Joystick\n");
			C->mode = RobotCommand_Mode_BEHAVIOR;
		}
		prevJoyRCEnabled = joyRCEnabled;

		// Create example user bytes to send back to computer
		for (int i = 0; i < GRM_USER_DATA_SIZE; ++i)
		{
			data.bytes[i] = i;
		}
		write(ETH_UPSTREAM_FILENO, &data, sizeof(UserData));
	}
};

Default defaultBehavior;

// You can optionally send back a fully custom state packet by replacing the state copy callback.
// If you do this, don't write user bytes using the example user bytes above.
uint16_t myStateCopyCallback(GRMHeader *hdr, uint8_t *buf)
{
	// Set your own version to keep track of parsing ability
	hdr->version = 1;
	int offset = 0;
	memcpy(buf, &S->millis, sizeof(S->millis));
	offset += sizeof(S->millis);

	// IMU
	memcpy(&buf[offset], &S->imu.angular_velocity, sizeof(Vector3));
	offset += sizeof(Vector3);
	memcpy(&buf[offset], &S->imu.euler, sizeof(Vector3));
	offset += sizeof(Vector3);

	// Need to return the size at the end
	return offset;
}

void debug()
{
	// Show joint positions
	for (uint8_t i = 0; i < P->joints_count; ++i)
		printf("%.2f\t", joint[i].getPosition());

	// Show raw toe values
	uint16_t rawToe[4];
	ioctl(TOE_SENSORS_FILENO, IOCTL_CMD_RD, rawToe);
	for (uint8_t i = 0; i < P->limbs_count; ++i)
		printf("%d\t", rawToe[i]);

	// Show IMU info
	// printf("%.2f\t%.2f\t%.2f\t", S->imu.euler.x, S->imu.euler.y, S->imu.euler.z);
	// printf("%.2f\t%.2f\t%.2f\t", S->imu.angular_velocity.x, S->imu.angular_velocity.y, S->imu.angular_velocity.z);
	// printf("%.2f\t%.2f\t%.2f\t", S->imu.linear_acceleration.x, S->imu.linear_acceleration.y, S->imu.linear_acceleration.z);

	printf("\n");

	// Update the ethernet and get the last update time
	ioctl(ETH_UPSTREAM_FILENO, IOCTL_CMD_GET_LAST_UPDATE_TIME, &ethRxUpdated);
}

int main(int argc, char *argv[])
{
	// Init Vision 60
	init(RobotParams_Type_NGR, argc, argv);

	// Enable remote control
	JoyType joyType = JoyType_FRSKY_XSR;
	ioctl(JOYSTICK_FILENO, IOCTL_CMD_JOYSTICK_SET_TYPE, &joyType);

	// Add our behavior
	behaviors.insert(behaviors.begin(), &defaultBehavior);
	defaultBehavior.begin();

// If physical robot
#if defined(ARM_MATH_CM4)
	bool ethernet_robot_control = true;
	if(ethernet_robot_control)
	{
		// Enable Robot Control SDK (via ethernet)
		mavCmd.begin();
		addPeripheral(&mavCmd);
	}
	else
	{ 
		// Replace state copy callback to send back custom logging info (via ethernet)
		ioctl(ETH_UPSTREAM_FILENO, IOCTL_CMD_STATE_COPY_CALLBACK, (void *)myStateCopyCallback);
	}	
#endif

	return begin();
}
