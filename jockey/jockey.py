import hopper_interface as hopif
import time
import numpy as np
import keyboard
import argparse
import pickle

description =   """
                This example couples the hopper with a virtual spring-mass-damper, otherwise known as a "jockey". The jockey
                is parameterized by a mass, natural frequency, damping ratio, and nominal spring length.
                The state of the jockey is simulated using Euler integration, and at each time step the interaction forces caused
                by the virtual spring and dash-pot are calculated and then output on the hopper via the haptic motor.
                """
parser = argparse.ArgumentParser(description=description)
parser.add_argument('mass', help='mass of the jockey in kilograms')
parser.add_argument('natural_frequency', help='the natural frequency of the spring-mass-damper system in Hz')
parser.add_argument('damping_ratio', help='the damping ratio of the spring-mass-damper system (dimensionless')
parser.add_argument('nominal_spring_len', help='the nominal length of the virtual spring in meters')

def parse_state(state):
    """ Helper function for parsing state into CSV """
    line = "%.4f,%.4f,%.4f,%.4f,%.4f,%.4f" % (state.leg_m1_angle, state.leg_m2_angle, state.haptic_m_angle, state.leg_m1_v, state.leg_m2_v, state.haptic_m_v)
    return line

class Jockey:
    """ Model of virtual coupled mass oscillator """
    def __init__(self, mass, natural_frequency, damping_ratio, l):
        self.m = mass
        self.natural_frequency = natural_frequency
        self.damping_ratio = damping_ratio
        self.k = (2*np.pi*self.natural_frequency)**2 * self.m
        self.b = 2*self.damping_ratio * (self.k*self.m)**.5
        self.nominal_spring_len = l  
        self.position = 0. 
        self.velocity = 0. 
        self.force = 0.
        self.last_position = 0.
        self.last_velocity = 0.

    def update(self, hopper_position, hopper_velocity, dt):
        self.force = self.k * (self.nominal_spring_len - self.position + hopper_position) - self.b * (self.velocity - hopper_velocity)
        new_velocity = self.velocity + (self.force/self.m-9.81) * dt
        new_position = self.position + self.velocity * dt
        self.setState(new_position, new_velocity)
        
    def setState(self, new_position, new_velocity):
        self.last_position = self.position
        self.position = new_position
        self.last_velocity = self.velocity
        self.velocity = new_velocity

    def getForce(self):
        return -self.force

# Open connection to hopper
hopper = hopif.HopperInterface('enp14s0')
hopper.setHapticMEffort(0.)
hopper.updateMotorCommand()

# Initialize state filter
state_filter = hopif.StateMedianFilter(3)

# Get initial hopper state
state = None
cnt = 0
print('initializing state estimate')
while True:
    state = hopper.getState()
    if state is not None:
        state_filter.insert(state)
        cnt+=1
    if cnt == state_filter._window_len:
        break

"""
Raise the robot to a specified height (as represented by the total angle of the haptic motor)
"""
ff_effort = .12
pulley_radius = 1.5 * 2.54 / 100
target_haptic_angle = .15/pulley_radius
feedback_controller = hopif.PID(0.04, 1e-4, 1e-4)
then = time.time()
while True:
    state = hopper.getState()
    if state is not None:
        now = time.time()
        dt = now - then
        state_filter.insert(state)
        median_state = state_filter.getMedian()
        error = target_haptic_angle - median_state.haptic_m_angle
        effort = feedback_controller.update(error, dt)  + ff_effort
        hopper.setHapticMEffort(effort)
        hopper.updateMotorCommand()
        if abs(error) < 1.e-1:
            break

# Store objects for analysis
pickle_data = {}

# create PIDS
m1_arial_pid = hopif.PID(.20, 0., 0.01)
m2_arial_pid = hopif.PID(.20, 0., 0.01)
retract_pid = hopif.PID(3.5,0.,.5)
pickle_data['m1_arial_pid'] = m1_arial_pid
pickle_data['m2_arial_pid'] = m2_arial_pid
pickle_data['retract_pid'] =  retract_pid

# Set gait parameters
# m1_extend_effort = -.8
# m2_extend_effort = .8
push_torque = 2.25
min_leg_len = hopif.percentToLength(0.25)
max_leg_len = hopif.percentToLength(.85)
print(max_leg_len)
descent_leg_len = hopif.percentToLength(.75)
print(descent_leg_len)
arial_sp = hopif.legCoordsToMotorAngles(descent_leg_len, np.pi/2)
m1_arial_sp = arial_sp[0]
m2_arial_sp = arial_sp[1]

pickle_data['push_torque'] = push_torque
pickle_data['max_leg_len'] = max_leg_len
pickle_data['descent_leg_len'] = descent_leg_len
pickle_data['m1_arial_sp'] = m1_arial_sp
pickle_data['m2_arial_sp'] = m2_arial_sp

time.sleep(1.)

# Reinitialize state and filter
state = None
cnt = 0
while True:
    state = hopper.getState()
    if state is not None:
        state_filter.insert(state)
        cnt+=1
    if cnt == state_filter._window_len:
        break

# Set the jockey initial conditions
pulley_radius = 1.5 * 2.54 / 100
median_state = state_filter.getMedian()

# Set up jockey model
args = parser.parse_args()
mass = float(args.mass)
natural_frequency = float(args.natural_frequency) 
damping_ratio = float(args.damping_ratio)
spring_len = float(args.nominal_spring_len)
jockey = Jockey(mass, natural_frequency, damping_ratio, spring_len)
hopper_position = pulley_radius * median_state.haptic_m_angle
hopper_velocity = pulley_radius * median_state.haptic_m_v
initial_position = hopper_position + jockey.nominal_spring_len - (9.81 * jockey.m / jockey.k)
initial_velocity = hopper_velocity
jockey.setState(initial_position, initial_velocity) 

# Open output file for data logging
filename = raw_input("Please provide filename: ")
f = open(filename, 'wb')
foo = raw_input("press anything to begin")


# initialize leg position
then = time.time()
while True:
    state = hopper.getState()
    if state is not None:
        now = time.time()
        dt = now-then
        then=now

        leg_len = hopif.getLegLength(state)
        length_error = max_leg_len - leg_len
        if abs(length_error) < 0.02:
            break
        m1_error = m1_arial_sp-state.leg_m1_angle
        m2_error = m2_arial_sp-state.leg_m2_angle
        m1_effort = m1_arial_pid.update(m1_error, dt)
        m2_effort = m2_arial_pid.update(m2_error, dt)
        print('m1 effort %f\n m2 effort %f' %(m1_effort,m2_effort))
        hopper.setLegM1Effort(m1_effort)
        hopper.setLegM2Effort(m2_effort)
        hopper.updateMotorCommand()

m1_arial_pid.reset()
m2_arial_pid.reset()

# drop thebot
print("beginning")
hopper.setHapticMEffort(0.)
hopper.updateMotorCommand()

hopper_state = "descend"

data = []
pickle_data['data'] = data
try:
    then = time.time()
    start_time = then
    while True:
        new_state = hopper.getState()

        if new_state is not None:
            now = time.time()
            dt = now - then
            then = now

            median_state = new_state
            angular_velocity = median_state.haptic_m_v
            hopper_position = pulley_radius * median_state.haptic_m_angle
            hopper_velocity = pulley_radius * angular_velocity
            leg_len = hopif.getLegLength(median_state)
            jockey.update(hopper_position, hopper_velocity, dt)
            force_desired = jockey.getForce()
            haptic_m_effort = hopif.force_lookup(angular_velocity
                                                , force_desired
                                                )
            hopper.setHapticMEffort(haptic_m_effort)
            leg_len = hopif.getLegLength(median_state)
            leg_v = hopif.getLegVelocity(median_state)

            if hopper_state == "descend":
                if hopper_position < .19:
                    print("Switching to retract state")
                    hopper_state = "retract"
                    retract_pid.reset()
                else:
                    m1_error = m1_arial_sp - median_state.leg_m1_angle
                    m2_error = m2_arial_sp - median_state.leg_m2_angle
                    m1_effort = m1_arial_pid.update(m1_error, dt)
                    m2_effort = m2_arial_pid.update(m2_error, dt)
                    hopper.setLegM1Effort(m1_effort)
                    hopper.setLegM2Effort(m2_effort)
            if hopper_state == "retract":
                if hopper_velocity > 0:
                    print("Switching to pushoff state")
                    hopper_state = "pushoff"
                else:
                    error = max_leg_len - leg_len
                    pid_output = retract_pid.update(error,dt)

                    m1_torque = -pid_output
                    m2_torque = pid_output
                    m1_current = m1_torque/.09532
                    m1_emf = .09532*state.leg_m1_v
                    m1_voltage = m1_current*.4357+m1_emf
                    m1_effort = m1_voltage/16.

                    m2_current = m2_torque/.09532
                    m2_emf = .09532*state.leg_m2_v
                    m2_voltage = m2_current*.4357+m2_emf
                    m2_effort = m2_voltage/16.

                    m1_effort = min(.99,max(-.99,m1_effort))
                    m2_effort = min(.99,max(-.99,m2_effort))
                    hopper.setLegM1Effort(m1_effort)
                    hopper.setLegM2Effort(m2_effort)
                    pass

            if hopper_state == "pushoff":
                if leg_len > max_leg_len:
                    hopper_state = "ascend"
                    print("Switching to ascend state")
                    m1_arial_pid.reset()
                    m2_arial_pid.reset()
                    hopper.setLegM1Effort(0)
                    hopper.setLegM2Effort(0)
                else:
                    m1_current = -push_torque/.09532
                    m1_emf = .09532*state.leg_m1_v
                    m1_voltage = m1_current*.4357+m1_emf
                    m1_effort = m1_voltage/16.

                    m2_current = push_torque/.09532
                    m2_emf = .09532*state.leg_m2_v
                    m2_voltage = m2_current*.4357+m2_emf
                    m2_effort = m2_voltage/16.

                    m1_effort = min(.99,max(-.99,m1_effort))
                    m2_effort = min(.99,max(-.99,m2_effort))
                    hopper.setLegM1Effort(m1_effort)
                    hopper.setLegM2Effort(m2_effort)

            if hopper_state == "ascend":
                if abs(hopper_velocity) < 1e-2:
                    hopper_state = "descend"
                    print("Switching to descend state")
                    m1_arial_pid.reset()
                    m2_arial_pid.reset()
                else:
                    hopper.setLegM1Effort(0)
                    hopper.setLegM2Effort(0)

            hopper.updateMotorCommand()
            data = parse_state(state)
            data += ',%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f\n' % (
                                                      leg_len
                                                    , leg_v
                                                    , jockey.last_position
                                                    , jockey.last_velocity
                                                    , force_desired
                                                    , m1_effort
                                                    , m2_effort
                                                    , haptic_m_effort
                                                    , now-start_time, dt
                                                    )
            m1_power = (16*m1_effort)*(16*m1_effort-.09352*median_state.leg_m1_v)/.4357
            m2_power = (16*m2_effort)*(16*m2_effort-.09352*median_state.leg_m2_v)/.4357
            data_element = ( hopper_state,
                             median_state,
                             hopper_position,
                             hopper_velocity,
                             leg_len,
                             leg_v,
                             jockey.last_position,
                             jockey.last_velocity,
                             force_desired,
                             m1_effort,
                             m2_effort,
                             haptic_m_effort,
                             m1_power + m2_power,
                             now
                             )
            pickle_data['data'] += [data_element]
        if keyboard.is_pressed('enter'):
            break

    # deactivate hopper
    hopper.stopAllMotors()
    time.sleep(1)
    hopper.disconnect()
    pickle.dump(pickle_data, f)
    f.close()

except BaseException as e:
    print("caught exception " + str(e) + "\n Stopping program now!")
    print(type(e))
    print(jockey.getForce())
    # deactivate hopper
    # try
    hopper.stopAllMotors()
    time.sleep(1)
    hopper.disconnect()
    pickle.dump(pickle_data, f)
    f.close()

