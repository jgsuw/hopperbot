import pickle
import numpy as np
from matplotlib import pyplot as plt 

pickle_data = None
with open('5hz_2.25effort','rb') as f:
    pickle_data = pickle.load(f)

data = pickle_data['data']

# Indices of each data element in the data tuples
hopper_state_idx = 0
motor_telemetry_idx = 1
hopper_position_idx = 2
hopper_velocity_idx = 3
leg_len_idx = 4
leg_v_idx = 5
jockey_position_idx = 6
jockey_velocity_idx = 7
force_idx = 8
m1_effort_idx = 9
m2_effort_idx = 10
haptic_effort_idx = 11
power_idx = 12
time_idx = 13

# Actual data
hopper_state = [x[0] for x in data]
motor_telemetry = [x[1] for x in data]
hopper_position = np.array([x[2] for x in data])
hopper_velocity = np.array([x[3] for x in data])
leg_len = np.array([x[4] for x in data])
leg_v = np.array([x[5] for x in data])
jockey_position = np.array([x[6] for x in data])
jockey_velocity = np.array([x[7] for x in data])
force = np.array([x[8] for x in data])
m1_effort = np.array([x[9] for x in data])
m2_effort = np.array([x[10] for x in data])
haptic_effort = np.array([x[11] for x in data])
power = np.array([x[12] for x in data])
time = np.array([x[13] for x in data])
time = time - time[0]
graph = [x[0] == 'descend' or x[0] == 'ascend' for x in data]
print(np.average(power))
plt.plot(time,hopper_position,'--o')
# plt.plot(time,jockey_position,'--o')
# plt.plot(time,leg_len,'--o')
plt.plot(time,graph)
plt.show()