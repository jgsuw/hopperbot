import setuptools
setuptools.setup(
        name='hopper_interface',
        version='0.1',
        author='Joseph Sullivan',
        author_email='jgs6156@uw.edu',
        description='Software API for controlling AMP lab hopper robot',
        license='FreeBSD',
)
