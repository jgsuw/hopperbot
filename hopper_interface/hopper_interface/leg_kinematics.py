"""
Not really sure what exactly is going to this file yet.
I was thinking that for starters, we could have a function that
returns the foot position and leg orientation given the joint angles.
"""

import scipy.optimize
import numpy as np
import time

L1 = 4 * .0254 # short link length in meters
L2 = 8 * .0254 # long link length in meters
foot_offset = 1.5 * .0254 # foot offset in meters

def _generateLookupTable(min_angle, max_angle, N):
    from pandas import DataFrame
    phi = np.linspace(min_angle, max_angle, num=N).reshape((N,1))
    r = computeLegLength(phi).reshape((N,1))
    solutions = DataFrame(np.append(r, phi, axis=1), columns=['r','phi'])
    sorted_LUT = solutions.sort_values('r')
    del solutions
    return sorted_LUT

def _lookup(r):
    lower_bound = None
    upper_bound = None
    for i in range(len(_length_LUT.values)):
        element = _length_LUT.values[i]
        if element[0] < r:
            lower_bound = i
        if element[0] > r:
            upper_bound = i
            break
    if lower_bound is None:
        return _length_LUT.values[0][1]
    elif upper_bound is None:
        return _length_LUT.values[-1][1]
    else:
        # Do a linear interpolation between the two bounds
        r0 = _length_LUT.values[lower_bound]
        r1 = _length_LUT.values[upper_bound]
        return (r1[1]-r0[1])/(r1[0]-r0[0])*(r-r0[0])+r0[1]

#TODO: the motor zeros and sign must be adjusted to reflect the correct coordinates

def getLegLength(motor_telemetry):
    beta = motor_telemetry.leg_m1_angle
    alpha = motor_telemetry.leg_m2_angle
    if beta > 0.:
        phi = (beta - alpha)/2
    elif alpha > 0.:
        phi = (beta + 2*np.pi - alpha)/2
    elif beta > alpha:
        phi = (beta - alpha)/2
    else:
        phi = (beta + 2*np.pi - alpha)/2
    phi = (beta-alpha) / 2
    if phi < 0.:
        phi += 2*np.pi
    return L1*np.cos(phi) + np.sqrt(L2**2-(L1*np.sin(phi))**2)

def computeLegLength(phi):
    return L1*np.cos(phi) + np.sqrt(L2**2-(L1*np.sin(phi))**2)

def getLegVelocity(motor_telemetry):
    beta = motor_telemetry.leg_m1_angle
    alpha = motor_telemetry.leg_m2_angle
    if beta > 0.:
        phi = (beta - alpha)/2
    elif alpha > 0.:
        phi = (beta + 2*np.pi - alpha)/2
    elif beta > alpha:
        phi = (beta - alpha)/2
    else:
        phi = (beta + 2*np.pi - alpha)/2
    beta_dot = motor_telemetry.leg_m1_v
    alpha_dot = motor_telemetry.leg_m2_v
    phi_dot = (beta_dot-alpha_dot) / 2
    
    triangle_equality = L2**2-(L1*np.sin(phi))**2
    term1 = -L1*np.sin(phi)
    term2 = -L1**2/2*np.sin(2*phi)/triangle_equality**(.5)
    return phi_dot * (term1 + term2)

def computeLegVelocity(phi, phi_dot):
    triangle_equality = L2**2-(L1*np.sin(phi))**2
    term1 = -L1*np.sin(phi)
    term2 = -L1**2/2*np.sin(2*phi)/triangle_equality**(.5)
    return phi_dot * (term1 + term2)

def getLegAcceleration(motor_telemetry, m1_torque, m2_torque):
    beta = motor_telemetry.leg_m1_angle
    alpha = motor_telemetry.leg_m2_angle
    if beta > 0.:
        phi = (beta - alpha)/2
    elif alpha > 0.:
        phi = (beta + 2*np.pi - alpha)/2
    elif beta > alpha:
        phi = (beta - alpha)/2
    else:
        phi = (beta + 2*np.pi - alpha)/2
    beta_dot = motor_telemetry.leg_m1_v
    alpha_dot = motor_telemetry.leg_m2_v
    phi_dot = (beta_dot-alpha_dot) / 2

    phi_dot_dot = (m1_torque-m2_torque)/2
    triangle_equality = L2**2-(L1*np.sin(phi))**2

    # term 1 is inertial component of acceleration
    term1 = -L1*np.sin(phi) - L1**2/2*np.sin(2*phi)/(triangle_equality)**(.5)
    # term 2 is the coriolis component of acceleration
    term2 = -L1*np.cos(phi)
    term2 += -L1**2*(np.cos(2*phi)/(triangle_equality)**(.5))
    term2 += -L1**4/4*(np.sin(2*phi)**2/(triangle_equality)**(1.5))
    return phi_dot_dot * term1 + term2 * phi_dot**2 


def computeLegAcceleration(phi, phi_dot, phi_dot_dot):
    triangle_equality = L2**2-(L1*np.sin(phi))**2
    # term 1 is inertial component of acceleration
    term1 = -L1*np.sin(phi) - L1**2/2*np.sin(2*phi)/(triangle_equality)**(.5)
    # term 2 is the coriolis component of acceleration
    term2 = -L1*np.cos(phi)
    term2 += -L1**2*(np.cos(2*phi)/(triangle_equality)**(.5))
    term2 += -L1**4/4*(np.sin(2*phi)**2/(triangle_equality)**(1.5))
    return phi_dot_dot * term1 + term2 * phi_dot_dot**2

def computeMotorAcceleration(r_dot_dot, theta_dot_dot
                            , m1_angle, m2_angle
                            , m1_v, m2_v
                            ):
    beta = m1_angle
    alpha = m2_angle
    if beta > 0.:
        phi = (beta - alpha)/2
    elif alpha > 0.:
        phi = (beta + 2*np.pi - alpha)/2
    elif beta > alpha:
        phi = (beta - alpha)/2
    else:
        phi = (beta + 2*np.pi - alpha)/2
    phi_dot = (m1_v - m2_v)/2
    theta = alpha + phi
    if theta < 0.:
        theta += 2*np.pi
    triangle_equality = L2**2-(L1*np.sin(phi))**2
    term1 = -L1*np.sin(phi) - L1**2/2*np.sin(2*phi)/(triangle_equality)**(.5)
    term2 = -L1*np.cos(phi)
    term2 += -L1**2*(np.cos(2*phi)/(triangle_equality)**(.5))
    term2 += -L1**4/4*(np.sin(2*phi)**2/(triangle_equality)**(1.5))
    phi_dot_dot = (r_dot_dot-term2*phi_dot**2)/term1

    m2_accel = theta_dot_dot-phi_dot_dot
    m1_accel = theta_dot_dot+phi_dot_dot
    return (m1_accel, m2_accel)

def motorTelemetryToLegCoords(motor_telemetry):
    beta = motor_telemetry.leg_m1_angle
    alpha = motor_telemetry.leg_m2_angle
    if beta > 0.:
        phi = (beta - alpha)/2
    elif alpha > 0.:
        phi = (beta + 2*np.pi - alpha)/2
    elif beta > alpha:
        phi = (beta - alpha)/2
    else:
        phi = (beta + 2*np.pi - alpha)/2
    theta = alpha + phi
    if theta < 0.:
        theta += 2*np.pi
    r = computeLegLength(phi)
    return (r, theta)

def motorAnglesToLegCoords(m1_angle, m2_angle):
    beta = m1_angle
    alpha = m2_angle
    if beta > 0.:
        phi = (beta - alpha)/2
    elif alpha > 0.:
        phi = (beta + 2*np.pi - alpha)/2
    elif beta > alpha:
        phi = (beta - alpha)/2
    else:
        phi = (beta + 2*np.pi - alpha)/2
    theta = alpha + phi
    if theta < 0.:
        theta += 2*np.pi
    r = computeLegLength(phi)
    return (r, theta)

def percentToLength(percent):
    if percent > .9:
        percent = .9
    elif percent < .25:
        percent = .25
    return (L2-L1) + 2*L1*percent

def lengthToPercent(length):
    return length/(L1+L2)

""" Objects used when transforming leg coords to motor angles """
_length_LUT = _generateLookupTable(0, np.pi, 10)
_f = lambda phi, r: L1*np.cos(phi) + (L2**2-(L1*np.sin(phi))**2)**(.5)-r
_df = lambda phi, r: -L1*np.sin(phi) - L1**2/2*np.sin(2*phi)*(L2**2-(L1*np.sin(phi))**2)**(-.5)
_solver = lambda r, phi: scipy.optimize.newton(_f, phi, fprime=_df, args=(r,))

def legCoordsToMotorAngles(r, theta):
    phi_guess = _lookup(r)
    phi = _solver(r, phi_guess)
    m1_angle = theta+phi
    m2_angle = theta-phi
    return (m1_angle, m2_angle)

if __name__ == "__main__":

    """ Here we test th speed of the inverse kinematics method. """
    N = 100000
    r = (L2-L1) + np.random.rand(N) * L1
    theta = np.pi*np.random.rand(N)
    import time
    dts = []
    for i in range(len(r)):
        start = time.time()
        legCoordsToMotorAngles(r[i], theta[i])
        end = time.time()
        dts.append(end-start)
    print('average time %f' % (np.average(dts),))
    print('min time %f' % (np.min(dts),)) 
    print('max time %f' % (np.max(dts),))
    print('std dev %f' % (np.std(dts),))