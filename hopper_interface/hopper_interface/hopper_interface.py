""" Author: Joseph Sullivan
email: jgs6156@uw.edu"""
import sys
#from .ethernet import MCU_SRC_PORT, headerFmt, headerSize
#from .ethernet import GRM_MAGIC, grmCrc
#from .ethernet import openTXSocket, openRXSocket, transmitPacket
from .ethernet import *
import struct
import socket
import time
from multiprocessing import Process, Queue, Event
import traceback
from numpy import median

PI = 3.14159265358979
HOPPER_PROTOCOL_VERSION = 42
LEG_M1_IDX = 5
LEG_M2_IDX = 7
HAPTIC_M_IDX = 6
JOEY_MAGIC = 31

def motorMask(idxs):
    # Helper function used by MotorCommand class
    mask = 0
    for idx in idxs:
        mask += 1 << idx
    return mask

class PID:
    """ Basic PID controller with integral decay """
    def __init__(self, p, i, d):
        self.p = p
        self.i = i 
        self.d = d 
        self.decay = 0.02
        self.integral = 0.
        self.last_error = None

    def update(self, error, dt):
        self.integral = (1-self.decay)*self.integral + dt * error
        derivative = 0.
        if self.last_error is not None:
            derivative = (error - self.last_error) / dt
        self.last_error = error
        output = self.p * error + self.i * self.integral + self.d * derivative
        return output
    
    def reset_integral(self):
        self.integral = 0.

    def reset_derivative(self):
        self.last_error = None

    def reset(self):
        self.integral = 0.
        self.last_error = None

class MotorTelemetry:
    """ This represents the state of the hopper motors (angle and velocity).
    Instances of this class are returned by HopperInterface.getState."""
    FORMAT_STR = '<ffffffI'
    def __init__(self, bstr):
        data = struct.unpack(MotorTelemetry.FORMAT_STR, bstr)
        self.leg_m1_angle = data[0]
        self.leg_m2_angle = data[1]
        self.haptic_m_angle = data[2]
        self.leg_m1_v = data[3]
        self.leg_m2_v = data[4]
        self.haptic_m_v = data[5]
        self.timestamp = int(data[6])

class MotorCommand:
    command_code = 42
    def __init__(self):
        self._mode_mask = 0
        self._control_vector = [0., 0., 0.]

    def setLegM1Effort(self, effort):
        """ Sets the duty ratio and direction (sign of torque).""" 
        self._mode_mask = self._mode_mask & ~(1<<LEG_M1_IDX)
        self._control_vector[0] = effort
        return self

    def setLegM2Effort(self, effort):
        """ Sets the duty ratio and direction (sign of torque).""" 
        self._mode_mask = self._mode_mask & ~(1<<LEG_M2_IDX)
        self._control_vector[1] = effort
        return self

    def setHapticMEffort(self, effort):
        """ Sets the duty ratio and direction (sign of torque).""" 
        self._mode_mask = self._mode_mask & ~(1<<HAPTIC_M_IDX)
        self._control_vector[2] = effort
        return self

    def _setLegM1Position(self, position):
        """ Deprecated."""
        self._mode_mask = self._mode_mask | (1<<LEG_M1_IDX)
        self._control_vector[0] = position
        return self

    def _setLegM2Position(self, position):
        """ Deprecated. """
        self._mode_mask = self._mode_mask | (1<<LEG_M2_IDX)
        self._control_vector[1] = position
        return self

    def _setHapticMPosition(self, position):
        """ Deprecated. """
        self._mode_mask = self._mode_mask | (1<<HAPTIC_M_IDX)
        self._control_vector[2] = position
        return self

    def packed(self):
        """ Returns a byte-array of the data in this class, in preparation for
        transmitting to mainboard."""
        self._raw_data = struct.pack('<3B3f',
                                    JOEY_MAGIC,
                                    MotorCommand.command_code,
                                    self._mode_mask,
                                    *self._control_vector)
        self._raw_data = self._raw_data + (34-len(self._raw_data)) * "\x00"
        return self._raw_data


def hopperReceivePacket(srx):
    """ This helper function unpacks binary data from the hopper. It
    originates from the GhostRobotics ethernet.py file, but has been
    modified to suit this interface."""
    data = None
    addr = [0, 0]
    while True:
        try:
            data_received, addr_received = srx.recvfrom(GRM_MAX_LENGTH)
        except:
            break
        data = data_received
        addr = addr_received
    if addr[1] != MCU_SRC_PORT:
        return None, None

    try:
        magic, version, totalSize, headerCrc, numDoF, configBits = struct.unpack(headerFmt, data[:headerSize])

        if magic != GRM_MAGIC:
            print('Magic byte did not match')
            return None, None

        # Check crc: it is contained in the header, and a crc32 of all the bytes after the header
        if headerCrc != grmCrc(data[headerSize: headerSize + totalSize]):
            print('CRC did not match')
            return None, None
        state = MotorTelemetry(data[headerSize:headerSize+totalSize])
    except BaseException as e:
        print(e)
        return None, None
    return state, numDoF

class HopperTxProcess(Process):
    """ This is a child process that is spawned by the HopperInterface. It
    buffers MotorCommands from the HopperInterface, and constantly retransmits
    the last received command to the main-board."""
    def __init__(self, iface):
        Process.__init__(self, target=self.target)
        self._iface = iface
        self._cmd_queue = Queue()
        self._exception_caught = Event()
        self._process_kill = Event()
        self._exception = Queue()
        try:
            self._tx_socket = openTXSocket(self._iface)
        except BaseException as e:
            self._exception_caught.set()
            self._exception.put(e)
            raise(e)

    def target(self):
        try:
            cmd = MotorCommand()
            while True:
                if not self._cmd_queue.empty():
                    cmd = self._cmd_queue.get()
                transmitPacket(self._tx_socket, cmd.packed())
                if self._process_kill.is_set():
                    break
        except BaseException as e:
            self._exception_caught.set()
            self._exception.put(e)

class StateMedianFilter:
    """ A median filter that is used to reject data glitches from the 
    mainboard. This was implemented after glitches were observed in the motor
    angles and angular velocity measurements."""
    def __init__(self, window_len):
        self._window_len = window_len
        self._window = [MotorTelemetry('\x00' * 28)] * window_len
        self._head = 0

    def insert(self, data):
        self._window[self._head] = data
        self._head = (self._head + 1) % self._window_len

    def getMedian(self):
        state = MotorTelemetry('\x00'*28)
        f = lambda x: x.leg_m1_angle
        state.leg_m1_angle = median(map(f, self._window))
        f = lambda x: x.leg_m2_angle
        state.leg_m2_angle = median(map(f, self._window))
        f = lambda x: x.haptic_m_angle
        state.haptic_m_angle = median(map(f, self._window))
        f = lambda x: x.leg_m1_v
        state.leg_m1_v = median(map(f, self._window))
        f = lambda x: x.leg_m2_v
        state.leg_m2_v = median(map(f, self._window))
        f = lambda x: x.haptic_m_v
        state.haptic_m_v = median(map(f, self._window))
        return state

    def reset(self, value):
        del self._window
        self._window = [value] * self._window_len

class HopperInterface:
    """
    This class implements the software side of the hopper API, and can be used
    to write an application that controls the hopper in closed-loop. To 
    instantiate this interface, the user must provide the name of the hopper
    network interface that is connected to the class constructor.
    """
    MAX_EFFORT = .95
    def __init__(self, iface):
        self._state = None
        self._last_m1_angle = 0.
        self._total_m1_angle = 0.
        self._last_m2_angle = 0.
        self._total_m2_angle = 0.
        self._last_haptic_m_angle = 0.
        self._total_haptic_angle = 0.
        self._cmd = MotorCommand()
        self._iface = iface
        self._rx_socket = None
        self._tx_process = HopperTxProcess(iface)
        self._connect()
        
    def setLegM1Effort(self, effort):
        """ Sets the duty ratio and direction (sign of torque) """ 
        if abs(effort) > HopperInterface.MAX_EFFORT:
            if effort > 0.:
                self._cmd.setLegM1Effort(HopperInterface.MAX_EFFORT)
            else:
                self._cmd.setLegM1Effort(-HopperInterface.MAX_EFFORT)
        else:
            self._cmd.setLegM1Effort(effort)

    def setLegM2Effort(self, effort):
        """ Sets the duty ratio and direction (sign of torque) """ 
        if abs(effort) > HopperInterface.MAX_EFFORT:
            if effort > 0.:
                self._cmd.setLegM2Effort(HopperInterface.MAX_EFFORT)
            else:
                self._cmd.setLegM2Effort(-HopperInterface.MAX_EFFORT)
        else:
            self._cmd.setLegM2Effort(effort)

    def setHapticMEffort(self, effort):
        """ Sets the duty ratio and direction (sign of torque) """ 
        if abs(effort) > HopperInterface.MAX_EFFORT:
            if effort > 0.:
                self._cmd.setHapticMEffort(HopperInterface.MAX_EFFORT)
            else:
                self._cmd.setHapticMEffort(-HopperInterface.MAX_EFFORT)
        else:
            self._cmd.setHapticMEffort(effort)

    def _setLegM1Position(self, position):
        """ Deprecated. """
        self._cmd._setLegM1Position(position)

    def _setLegM2Position(self, position):
        """ Deprecated. """
        self._cmd._setLegM2Position(position)

    def _setHapticMPosition(self, position):
        """ Deprecated. """
        self._cmd._setHapticMPosition(position)

    def stopAllMotors(self):
        """ Sets all motor PWM to zero, making the robot safe."""
        self.setLegM1Effort(0)
        self.setLegM2Effort(0)
        self.setHapticMEffort(0)
        self.updateMotorCommand()

    def updateMotorCommand(self):
        """ Updates the outgoing motor command. The user must call this
        function after changing motor commands for them to be sent."""
        if self._tx_process._exception_caught.is_set():
            e = self._tx_process._exception.get()
            self._tx_process._process_kill.set()
            print("Hopper Interface TX process caught exception!")
            raise(e)
        self._tx_process._cmd_queue.put(self._cmd)

    def getState(self):
        """ This retrieves the latest state (motor angles, velocity) from the
        robot in the form of a MotorTelemetry object. If no state data is
        retrieved due to a communication exception, then this returns None."""
        state, ndof = hopperReceivePacket(self._rx_socket) # ndof is not used.
        if state is not None:
            if self._state is None:
                """ Initialization of state. """
                self._state = state
                if state.leg_m1_angle < 0.:
                    state.leg_m1_angle += 2*np.pi
                self._last_m1_angle = state.leg_m1_angle
                self._total_m1_angle = state.leg_m1_angle
                self._last_m2_angle = state.leg_m2_angle
                self._total_m2_angle = state.leg_m2_angle
                self._last_haptic_m_angle = state.haptic_m_angle
                self._total_haptic_angle = 0.
                return self._state
            else:
                if self._state.timestamp == state.timestamp:
                    return None
                self._state.timestamp = state.timestamp
                delta = self._getDeltaAngle(state.leg_m1_angle
                                            , self._last_m1_angle
                                            )
                self._total_m1_angle += delta
                self._last_m1_angle = state.leg_m1_angle

                delta = self._getDeltaAngle(state.leg_m2_angle
                                            , self._last_m2_angle
                                            )
                self._total_m2_angle += delta
                self._last_m2_angle = state.leg_m2_angle

                delta = self._getDeltaAngle(state.haptic_m_angle
                                            , self._last_haptic_m_angle
                                            )
                self._total_haptic_angle += delta
                self._last_haptic_m_angle = state.haptic_m_angle
                
                self._state.leg_m1_angle = self._total_m1_angle
                self._state.leg_m2_angle = self._total_m2_angle
                self._state.haptic_m_angle = self._total_haptic_angle
                
                self._state.leg_m1_v = state.leg_m1_v
                self._state.leg_m2_v = state.leg_m2_v
                self._state.haptic_m_v = state.haptic_m_v

                return self._state
        else:
            return None

    def disconnect(self):
        """ Kills the TX child process, stopping all control transmission """
        print("Hopper Interface is disconnecting\n")
        if self._tx_process._exception_caught.is_set():
            e = self._tx_process._exception.get()
            print("\n" + str(e))
            print("Hopper Interface TX process caught exception")
        self._tx_process._process_kill.set()
        self._tx_process.join()

    def _connect(self):
        self._rx_socket = openRXSocket()
        self._rx_socket.setblocking(0)
        self._tx_process.start()

    def _getDeltaAngle(self, new_angle, old_angle):
        """ Helper method used to unwrap angles """
        delta = new_angle-old_angle
        if delta > PI:
            delta = new_angle-(old_angle+2*PI)
        elif delta < -PI:
            delta = new_angle-(old_angle-2*PI)
        return delta
